#! bin/bash 

# On test si on est root
test=`whoami`
if [ $test != "root" ]; 

then

  echo "[/!\] Vous devez être super-utilisateur [/!\]"
  exit

fi

echo "[...] update database [...]"

apt-get update 

echo "[...] upgrade system [...]"

apt-get -y dist-upgrade #-y dira automatiquement oui si on nous propose un choix, dist-upgrade choisira intelligemment les paquets les plus importants en évitant les conflits

apt-get clean #pour supprimer les paquets recuperes, ça supprime tout et bloque le fichier dans /var/cache/apt/archives/partial et /var/cache/apt/archives/

apt-get autoremove --purge #ça montrera avec une petite etoile, les fichiers qui auraient ete supprimes sans l utilisation de la commande pour laisser le choix a l utilisateur de les supprimer ou non

exit 0

