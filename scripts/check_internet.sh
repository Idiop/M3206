#! bin/bash

	echo "[...] Checking internet connection [...]"

ping -c3  www.google.fr   #On ping vers google pour check le bon fonctionnement de TCP/IP et la résol DNS

TEST=$?  

if [ $TEST -ne 0 ] #La fonction -ne va faire en sorte que $TEST soit différent de 0

    then

	echo "[...] Not connected to Internet	 [/!\]"
	echo "[/!\] Please check configuration   [/!\]"  

    else

        echo "[...] Internet access OK		 [...]"                                            │

fi

