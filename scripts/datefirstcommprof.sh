#!/bin/bash

#on crée une variabe timestamp, on fait afficher le fichier choisi
#on affiche la deuxième colonne avec awk'{print $2}'
#grep $1 permet de garder les lignes qui continennent la commande donné en paramètre
#cut -d couler la chaîne de caractère au :

#-f1 garde la partie avant le :
#head -1 montre la première commande tapée

tstamp=$(cat $2 | awk '{print $2}' | grep $1 | cut -d ':' -f1 | head -1)

#affiche la date à laquelle a été tapé la commande
#le -d converti le nombre en quelque chose de compréhensible

date -d @$tstamp	
