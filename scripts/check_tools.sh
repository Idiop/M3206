#! bin/bash

#On crée des boucles si pour savoir si le programme spécifié fonctionne ou pas, avec which "nom du programme" > /dev/null , on va à l'aide du which retourner le chemin des programmes ou fichiers qui seraient executés dans l'environnement actuel et renvoyé vers le vide /dev/null, pour éviter de répéter la commande 

if which git >/dev/null; then
    echo "[...] git exists	   [...]"
else
    echo "[/!\] vim does not exist [/!\]" 
fi

if which tmux >/dev/null; then
    echo "[...] tmux exists	   [...]"
else
    echo "[/!\] tmux does not exist[...]"
fi

if which vim >/dev/null; then
    echo "[...] vim exists	   [...]"
else
    echo "[...] vim does not exist [/!\]"
    apt-get install vim
fi

if which htop >/dev/null; then
    echo "[...] htop exists	   [...]"
else
    echo "[/!\] htop does not exist[/!\]"
    echo "[...] Installing htop    [...]"
    apt-get install htop

fi
