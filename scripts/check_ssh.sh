#! /bin/bash

#On installe et construit les paquets relatifs à SSH qu'on redirige vers null pour pas que ça s'affiche à l'écran, on redirige e standard output stdout qui est représenté par le 1, vers le standard error stder qui est représenté par le 2
  
dpkg -l ssh >>/dev/null 2>&1
 

if [ $? -eq 0 ]; then
	echo "[...] ssh: est installé [...]"
else 
	echo "[/!\] ssh: n'est pas installé [/!\]"

fi
#Va montrer tous les process en cours etc (aux), et les envoyer vers null pour pas que ça s'affiche à l'écran 

ps aux|grep [s]shd >> /dev/null 2>&1 

if [ $? -eq 0 ]; then
	echo "[...] ssh: le service est lancé [...]"
else
	echo "[/!\] ssh: le service n'est pas lancé  [/!\]"
	echo "[...] ssh: lancement du service	    [...]"
	/etc/ini.d/ssh start
fi

exit 0 
