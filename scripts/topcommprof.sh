#!/bin/bash 

##awk '{print $2}' permet de n'afficher que la deuxième colonne de la sortie
#cut -d permet de couper en fonction d'une chaine de caractère
	
#l'option -f2 permet de garder ce qui est après la chaine de caractère
#sort permet de trier la sortie par par ordre alphabétique
#uniq -c permet de compter le nombre d'occurences
#sort -nr permet de mettre dans l'ordre numérique croissant puis d'inverser cet ordre avec -r en num décroissant
#head -$1 permet de n'avoir que les x premières lignes données en paramètres
#awk '{print $1" "$2}' permet d'avoir une sortie plus "propre" , cette commande est éventuellement facultative

cat my_history | awk '{print $2}' | cut -d ';' -f2 | sort | uniq -c | sort -nr | head -$1 


                                               
