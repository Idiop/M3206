
TD1)

Exos1)

1) La commande git status va afficher les "changemements commis" dans une branche.

2) On crée une ligne dans un fichier TD1.git.tt qu on a cree, et lorsque l'on effectue un git status il nous présente le dernier changement effectué.

Exo2)

la commande git log renvoie les commits effectué sur une période (que je n'ai pas pu délimiter puisque cela fait trop peu de temps que j'utilise le gitlab) en précisant le titre du commit et la date.

Exo3) 

On ajoute une cinquième ligne dans le fichier TD1_Git.txt, ainsi lorsque l'on demande le statut du git, il nous montre normalement que le fichier à été modifié; Or en revant un pas en arrière avec la commande git checkout -- TD1_Git.txt, la dernière modification ayant apparue dans le fichier disparaît, donc lorsque l'on check son statut, il n'apparaît plus comme fichier modifié.

Exo 4)

La commande git status va retourner que le fichier est prêt à être commité. Or on retire le fichier de l'index et on supprime la dernière modification effectuée dans le fichier, donc la cinqiuème ligne disparaît de nouveau et il n'y a plus le fichier TD1 a l'index

Exo 7) 

Le fichier TD1 ne contient pas la ligne ajoutée depuis la branche TEST
Le fichier TD contient désormais la ligne ajoutée depuis la branche TEST avec l'utilisation de la commande git merge TEST. Le git log considère le fait d'avoir inclu les modifications faites dans la branche TEST à la branche master par défaut, comme un commit, ainsi elle apparaît dans le log.

TD2)

Pour l'exercice 4 on utilise le raccourci r qui va remplacer quelque chose, en l'occurrence, on insère le nombre de caractères avant le r donc ici le nombre de caractères est 99 donc on va taper 99 r i pour remplacer les 99 caractères par i, puis appuyer de nouveau sur i pour rajouter le 100e i.
